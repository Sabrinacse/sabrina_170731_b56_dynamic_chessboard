Command line instructions


Git global setup

git config --global user.name "Sabrina Sharmin"
git config --global user.email "sabrinacse14@gmail.com"

Create a new repository

git clone https://Sabrinacse@gitlab.com/Sabrinacse/sabrina_170731_b56_dynamic_chessboard.git
cd sabrina_170731_b56_dynamic_chessboard
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://Sabrinacse@gitlab.com/Sabrinacse/sabrina_170731_b56_dynamic_chessboard.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://Sabrinacse@gitlab.com/Sabrinacse/sabrina_170731_b56_dynamic_chessboard.git
git push -u origin --all
git push -u origin --tags